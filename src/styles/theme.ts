import { extendTheme } from "@chakra-ui/react";

// example theme
export const theme = extendTheme({
  styles: {
    global: {
      body: {
        bg: "gray.900", //backgroundColor
        color: "gray.50",
      },
    },
  },
  fonts: {
    heading: "Roboto",
    body: "Roboto",
  },
});
