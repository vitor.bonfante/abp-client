import { Button, ButtonProps } from "@chakra-ui/react";

export interface ButtonIconProps extends ButtonProps {
  text?: string;
  icon: any;
}
