import { useNavigate } from "react-router-dom";
import { useAuth } from "../../context/AuthProvider/useAuth";

export const ProtectedLayout = ({ children }: { children: any }) => {
  const auth = useAuth();
  let navigate = useNavigate();

  if (!auth.email) {
    return <h1>Não autorizado</h1>;
  }
  return children;
};
