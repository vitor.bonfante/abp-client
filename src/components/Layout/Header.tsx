import { Flex, Text, Input, Icon, HStack, Box, Avatar } from "@chakra-ui/react";
import {
  RiSearchLine,
  RiNotificationLine,
  RiUserAddLine,
} from "react-icons/ri";

export function Header() {
  return (
    <Flex
      as="header"
      w="100%"
      maxWidth={1480}
      h="20"
      mx="auto"
      mt="4"
      px="6"
      align="center"
    >
      <Text fontSize="3x1" fontWeight="bold" letterSpacing="tight" w="64">
        dashgo
        <Text as="span" ml="1" color="pink.500">
          .
        </Text>
      </Text>

      <Flex
        as="label"
        flex="1"
        py="4"
        px="8"
        ml="6"
        maxWidth={400}
        align="center"
        color="gray.200"
        position="relative"
        bg="gray.800"
        borderRadius="full"
      >
        <Input
          color="gray.50"
          variant="unstyled"
          px="4"
          mr="4"
          placeholder="Digite sua busca"
          _placeholder={{ color: "gray.400" }}
        />
        <Icon as={RiSearchLine} />
      </Flex>

      <Flex
        align="center"
        ml="auto"
        mx="8"
        pr="8"
        py="1"
        color="gray.300"
        borderRightWidth={1}
        borderColor="gray.700"
      >
        <HStack>
          <Icon as={RiNotificationLine} fontSize="20px" />
          <Icon as={RiUserAddLine} fontSize="20px" />
        </HStack>
      </Flex>
      <Flex align="center">
        <Box>
          <Text mr="4" textAlign="right">
            Carlos Daniel
          </Text>
          <Text color="gray.300" fontSize="small">
            carlos.pasquali@gmail.com
          </Text>
        </Box>
        <Avatar
          size="md"
          name="Carlos Daniel"
          src="https://github.com/diego3g.png"
        />
      </Flex>
    </Flex>
  );
}

/*


//REACT
import { useEffect, useState } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
//FUNCTIONS
import { siderNavigate, getItem } from "./util";

//TYPES
import { MenuItem, BreadcrumbHistory } from "../../types/Layout";

export const LayoutSider = () => {
  let navigate = useNavigate();

  const [collapsed, setCollapsed] = useState<boolean>(true);
  const [selectedKey, setSelectedKey] = useState<string>("/profile");
  const [breadcrumbHistory, setBreadcrumbHistory] =
    useState<BreadcrumbHistory[]>();

  useEffect(() => {
    siderNavigate(setBreadcrumbHistory, setSelectedKey);
  }, [window.location.pathname]);

  const items: MenuItem[] = [
    getItem("Usuários", "/users", <TeamOutlined />),
    getItem("Configurações", "/profile", <ToolOutlined />),
  ];

  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={() => setCollapsed(!collapsed)}
      >
        <Space
          style={{
            margin: "14px 0px 12px 0px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Avatar
            shape="square"
            size={48}
            src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
          />
        </Space>
        <Menu
          theme="dark"
          selectedKeys={[selectedKey]}
          mode="inline"
          items={items}
          onClick={(evt) => {
            navigate(evt.key);
          }}
        />
      </Sider>

      <Layout className="site-layout">
        <Content style={{ margin: "0 16px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            {breadcrumbHistory?.map((element, i: number) => {
              return (
                <Breadcrumb.Item key={i}>
                  <Link to={element.url}>{element.title}</Link>
                </Breadcrumb.Item>
              );
            })}
          </Breadcrumb>

          <Outlet />
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};



*/
