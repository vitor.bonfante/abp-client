//UI
import { Avatar, Flex, Spacer } from "@chakra-ui/react";
import {
  RiGroupLine,
  RiSettings3Line,
  RiUserReceived2Line,
} from "react-icons/ri";

//NAVEGATION
import { useNavigate } from "react-router-dom";
import { SidebarProps } from "../../../types/Sidebar";

//COMPONENTS
import { ButtonIcon } from "../Buttons/ButtonIcon";

export function SidebarContent({ bg }: SidebarProps) {
  let navigate = useNavigate();

  const Items = [
    { icon: RiGroupLine, text: "Usuários", url: "/users" },
    { icon: RiSettings3Line, text: "Configurações", url: "/profile" },
    { icon: RiUserReceived2Line, text: "Sair", url: "/login" },
  ];

  return (
    <Flex
      id="Sidebar-Content"
      width="100%"
      h="100%"
      bg={bg}
      direction="column"
      align="center"
      borderEndRadius="10px"
      p={3}
    >
      <Avatar size="lg" borderRadius="10px" src="https://bit.ly/dan-abramov" />
      {Items.map((element, i: number) => {
        return (
          <>
            {i === Items.length - 1 && <Spacer h="100%" />}
            <ButtonIcon
              colorScheme="blue"
              icon={element.icon}
              text={element.text}
              mt={3}
              onClick={() => {
                navigate(element.url);
              }}
            />
          </>
        );
      })}
    </Flex>
  );
}
