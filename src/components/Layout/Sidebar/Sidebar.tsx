import { Flex } from "@chakra-ui/react";

import { Outlet } from "react-router-dom";

import { SidebarBody } from "./SidebarBody";

export function Sidebar() {
  return (
    <Flex h="100vh" bg="gray">
      <SidebarBody bg="gray.500" />
      <Flex id="App-Content" w="100%">
        <Outlet />
      </Flex>
    </Flex>
  );
}
