import {
  Box,
  Button,
  Flex,
  Icon,
  Slide,
  useDisclosure,
  VStack,
} from "@chakra-ui/react";
import { SidebarContent } from "./SidebarContent";

import { motion } from "framer-motion";
import { useState } from "react";

import { RiArrowDropRightLine } from "react-icons/ri";
import { SidebarProps } from "../../../types/Sidebar";

export function SidebarBody({ bg }: SidebarProps) {
  const [hidden, setHidden] = useState<boolean>(true);

  return (
    <motion.div
      onMouseEnter={() => {
        setHidden(true);
      }}
      onMouseLeave={() => {
        setHidden(false);
      }}
      animate={{ left: hidden ? 0 : -92 }}
      style={{
        position: "absolute",
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        height: "100vh",
      }}
    >
      <SidebarContent bg={bg} />
      <Flex align="center" bg={bg} w="20px" h="20%" borderEndRadius="10px">
        <Icon w={6} h={6} as={RiArrowDropRightLine} />
      </Flex>
    </motion.div>
  );
}
