import { MenuItem } from "../../types/Layout";

export function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[]
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

export function siderNavigate(setBreadcrumbHistory: any, setSelectedKey: any) {
  let tempUrl = window.location.pathname.split("/");
  tempUrl.shift();
  setSelectedKey("/" + tempUrl[0]);

  let breadcrumb = [];
  let tempPath = "";

  for (let i = 0; i < tempUrl.length; i++) {
    tempPath += "/" + tempUrl[i];
    breadcrumb.push({ url: tempPath, title: tempUrl[i] });
  }
  setBreadcrumbHistory(breadcrumb);
}
