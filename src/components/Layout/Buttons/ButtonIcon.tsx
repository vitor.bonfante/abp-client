import { Icon, Text, Button, Tooltip } from "@chakra-ui/react";

//Types

import { ButtonIconProps } from "../../../types/Buttons";

export function ButtonIcon({ text, icon, ...rest }: ButtonIconProps) {
  return (
    <Tooltip placement="auto" label={text}>
      <Button flexDirection="column" {...rest}>
        <Icon w={6} h={6} as={icon} />
        {/*
       <Text fontSize="xs">{text}</Text>
      */}
      </Button>
    </Tooltip>
  );
}
