import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
//Components
import { AuthProvider } from "../context/AuthProvider";
import { ProtectedLayout } from "../components/ProtectedLayout";
import { Header } from "../components/Layout/Header";
import { Sidebar } from "../components/Layout/Sidebar/Sidebar";

//NotFound
import { NotFound } from "../screens/NotFound";

//Profile
import { Profile } from "../screens/Profile";

//Access
import { Login } from "../screens/Access/Login";
import { Register } from "../screens/Access/Register";

//Users

import { UsersList } from "../screens/User/List";

import { UserDetails } from "../screens/User/Details";

function AppRoutes() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />

          <Route path="*" element={<NotFound />} />

          <Route
            path="/"
            element={
              // <ProtectedLayout>
              <Sidebar />
              //</ProtectedLayout>
            }
          >
            <Route path="/profile" element={<Profile />} />

            <Route path="/users" element={<Outlet />}>
              <Route index element={<UsersList />} />
              <Route path="details" element={<UserDetails />} />
            </Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default AppRoutes;
