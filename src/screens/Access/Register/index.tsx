import { useAuth } from "../../../context/AuthProvider/useAuth";
import { useNavigate } from "react-router-dom";

export const Register = () => {
  const auth = useAuth();
  let navigate = useNavigate();

  async function onFinish(values: { email: string; password: string }) {
    try {
      await auth.authenticate(values.email, values.password);

      navigate(`/profile`);
    } catch (error) {
      alert("Invalid email or password");
    }
  }
  return <div>register</div>;
};
