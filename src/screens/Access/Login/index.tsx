import { useAuth } from "../../../context/AuthProvider/useAuth";
import { useNavigate } from "react-router-dom";
import { Button, Flex, Stack } from "@chakra-ui/react";

import { Input } from "../../../components/Form/Input";

export const Login = () => {
  const auth = useAuth();
  let navigate = useNavigate();

  async function onFinish(values: { email: string; password: string }) {
    try {
      console.log(values);
      await auth.authenticate(values.email, values.password);

      navigate(`/profile`);
    } catch (error) {
      alert("Invalid email or password");
    }
  }
  return (
    <Flex w="100vw" h="100vh" align="center" justify="center">
      <Flex
        as="form"
        width="100%"
        maxWidth="360px"
        bg="gray.800"
        p="8"
        borderRadius="8px"
        flexDir="column"
      >
        <Stack spacing="4">
          <Input name="email" type="email" label="E-mail" />
          <Input name="password" type="password" label="Senha" />
        </Stack>

        <Button type="submit" mt="6" colorScheme="pink">
          Entrar
        </Button>
      </Flex>
    </Flex>
  );
};
